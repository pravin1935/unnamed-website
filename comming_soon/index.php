<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<style type="text/css">
  
  html {
  position: relative;
  min-height: 100%;
}

body {
  text-align: center;
  background-image: url("store.png");
  background-size: cover;
  background-attachment: fixed;
  background-repeat: no-repeat;
  font-family: 'Open Sans Condensed', sans-serif;
}

.overlay {
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  width: 100%;
  height: 100%;
  z-index: -1;
  overflow: hidden;
  background: rgba(23, 22, 22, 0.72)
}

.content {
  width: 75%;
  position: relative;
  margin: 2% auto;
  padding: 8px;
}

.content h1 {
  font-size: 3em;
  color: #fff;
  letter-spacing: 3px;
  padding: 25px 0px;
  text-transform: uppercase;
}

.content .input-group {
  margin: 0 auto;
  width: 75%;
}

.content p {
  color: #fff;
  line-height: 30px;
  padding-bottom: 12px;
}

@media (max-width: 450px) {
  .content h1 {
    letter-spacing: 2px;
    font-size: 25px;
  }
  .content .input-group {
    margin: 0 auto;
    width: 100%;
  }
}

.content ul.social {
  margin-top: 45px;
}

.content li a {
  color: #fff;
  margin: 2%;
}


</style>
<body>
<div class="overlay"></div>
<div class="wrap">
  <div class="content">
    <h1><i class="fa fa-rocket" aria-hidden="true"></i></h1>
    <p>PARACOSMA is </p>
    <h3 style="color: white;">building <span style="color:#fb0;">UNNAMED</span></h3>
    <p>we are <strong>working on something</strong> interesting. Keep visiting us </p>
 <h3>
     
      </h3>

      <h1 style="border:1px solid">COMMING <span style="color:#fb0;">SOON</span></h1>  
    <ul class="social list-inline">
      <li><h2><a href="https://www.facebook.com/Unnamed-784058948440506/"><i class="fa fa-facebook-square" aria-hidden="true"></i>
    </a>
  </h2></li>
      
    </ul>

     <div class="input-group input-group-lg">

      <input id="emailadd" name="emailadd" type="email" placeholder="Subscribe here for regular update..." class="form-control" required />
      <span class="input-group-btn">
      <button id="btn-ok" class="btn btn-warning">OK!</button>
      </span>

         
    </div>

  </div>
  

</div>
<!-- END #WRAP -->

<script type="text/javascript">
  /* Under Construction Template
 * by: Aleks Bella
 */

$(function(){

  
  $('#btn-ok').on('click',function(){
   
    if($('#emailadd').val().length === 0){
      alert('Please enter your email to subscribe.');
    }else{
       $(".input-group-lg").css('color', '#f90').html("Thanks for the subscription.");
      $("#myModal").modal("show");
    }
    
    
  });
  
});
</script>
 <script type="text/javascript" src="https://embed.modernapp.co/chat?code=92c4a69006a13b2949bd5d917e37795a"></script>
</body>
</html>